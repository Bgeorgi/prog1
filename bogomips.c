#include <time.h>
#include <stdio.h>
#include <math.h>

void delay (unsigned long long int loops)
{
unsigned long long int i;

for ( i=0; i<loops; i++);
}

int main(void)
{
unsigned long long int loops_per_sec = 1;
unsigned long long int ticks;

printf ("Calibrating delay loop..");
fflush(stdout);

while ((loops_per_sec <<= 1 ))
{
ticks = clock();
delay (loops_per_sec);
ticks = clock() - ticks;

printf ("%llu %llu\n", ticks, loops_per_sec);

if (ticks >= CLOCKS_PER_SEC)
{
double d = ((double)loops_per_sec / (double)ticks) * (double)CLOCKS_PER_SEC;

printf ("ok %f %f BogoMIPS\n", d, log(d));

return 0;
}
}

printf ("failed\n");
return -1;
}